﻿using Newtonsoft.Json;

namespace Checkers {

    [JsonObject]
    public class Cell {
        private int r, c;

        private Piece piece;
        public Cell(int r, int c, Piece piece) {
            this.r = r;
            this.c = c;
            this.piece = piece;
        }
        public bool ShouldSerializePiece() { return false; }

        public virtual int R {
            set {
                r = value;
            }
            get {
                return r;
            }
        }

        public virtual int C {
            set {
                c = value;
            }
            get {
                return c;
            }
        }

        public virtual Piece Piece {
            set { piece = value; }
            get { return piece; }
        }


    }

}