﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Checkers
{
    public class GameInfo
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Mode mode { set; get; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Level level { set; get; }
        [JsonIgnore]
        public bool init { set; get; }
    }
}
