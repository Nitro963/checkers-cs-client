﻿namespace Checkers.Constraints {
    public interface IConstraint {
        bool isValid(Player player);
    }

}