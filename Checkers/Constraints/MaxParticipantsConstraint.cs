﻿using Newtonsoft.Json;

namespace Checkers.Constraints {

    [JsonObject]
    public class MaxParticipantsConstraint : IConstraint {
        private int MaxParticipant { set; get; }
        private int NumOfParticipants { set; get; }

        public MaxParticipantsConstraint(int maxParticipant, int numOfParticipants) {
            this.MaxParticipant = maxParticipant;
            this.NumOfParticipants = numOfParticipants;
        }

        public virtual bool isValid(Player player) {
            return NumOfParticipants + 1 <= MaxParticipant;
        }
    }

}