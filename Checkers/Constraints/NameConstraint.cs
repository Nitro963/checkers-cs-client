﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace Checkers.Constraints {

    [JsonObject]
    public class NameConstraint : IConstraint {
        private List<string> Names { set; get; }

        public NameConstraint() {
            Names = new List<string>();
        }
        public NameConstraint(List<string> Names) {
            this.Names = Names;
        }

        public virtual bool isValid(Player player) {
            foreach (string name in Names) {
                if (player.Name.Equals(name)) {
                    return true;
                }
            }

            return false;
        }
    }

}