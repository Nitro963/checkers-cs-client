﻿using Newtonsoft.Json;
using System;

namespace Checkers.Constraints {

    [JsonObject]
    public class DateConstraint : IConstraint {
        private DateTime Date { set; get; }

        public DateConstraint(DateTime Date) {
            this.Date = Date;
        }

        public virtual bool isValid(Player player) {
            var currentDate = DateTime.UtcNow;
            return currentDate <= Date;
        }
    }

}