﻿using Newtonsoft.Json;

namespace Checkers.Constraints {
    [JsonObject]
    public class RatingConstraint : IConstraint {
        private readonly int Min, Max;

        public RatingConstraint() {
            Min = int.MinValue;
            Max = int.MaxValue;
        }

        public RatingConstraint(int Min, int Max) {
            this.Min = Min;
            this.Max = Max;
        }

        public virtual bool isValid(Player player) {
            if (player.Rate >= Min && player.Rate <= Max) {
                return true;
            }
            return false;
        }
    }

}