﻿using Newtonsoft.Json;

namespace Checkers {
    [JsonObject]
    public class Action {
        private int id;
        private Cell src;
        private Cell dst;
        private Player player;
        private Piece capture;
        private bool promote;
        private int no_progress_count;

        public Action(Cell src, Cell dst, Player player) {
            this.src = src;
            this.dst = dst;
            this.player = player;
            capture = null;
            id = -1;
        }

        [JsonConstructor]
        public Action(int id, Cell src, Cell dst, Player player, Piece capture, bool promote, int no_progress_count) {
            this.src = src;
            this.dst = dst;
            this.player = player;
            this.capture = capture;
            this.id = id;
            this.promote = promote;
            this.no_progress_count = no_progress_count;
        }

        public virtual int Id {
            get {
                return id;
            }
            set {
                this.id = value;
            }
        }
        public virtual Cell Src {
            get {
                return src;
            }
            set {
                this.src = value;
            }
        }
        public virtual Cell Dst {
            get {
                return dst;
            }
            set {
                this.dst = value;
            }
        }
        public virtual Player Player {
            get {
                return player;
            }
            set {
                this.player = value;
            }
        }
        public virtual Piece Capture {
            get {
                return capture;
            }
            set {
                this.capture = value;
            }
        }

        public virtual bool Promote {
            get {
                return promote;
            }
            set {
                this.promote = value;
            }
        }

        public virtual int NoProgressCount {
            get {
                return no_progress_count;
            }
            set {
                this.no_progress_count = value;
            }
        }

        override
        public string ToString() {
            return "(" + (src.R + 1).ToString() + "," + (src.C + 1).ToString() + ")" + " -> "
                    + "(" + (dst.R + 1).ToString() + "," + (dst.C + 1).ToString() + ")";
        }

    }

}