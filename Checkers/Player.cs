﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Checkers {
    [JsonObject]
    public class Player {
        private int id;
        private int rate;
        private string name;
        private string password;
        private List<Contest> currentContests;

        public Player(int id, string name, string password) {
            this.id = id;
            this.name = name;
            this.password = password;

            rate = 1000;
            currentContests = new List<Contest>();
        }
        [JsonConstructor]
        public Player(int id, string name, string password, int rate)
        {
            this.id = id;
            this.name = name;
            this.password = password;
            this.rate = rate;
            currentContests = new List<Contest>();
        }

        public Player(string name, string password)
        {
            this.name = name;
            this.password = password;

            rate = 1000;
            currentContests = new List<Contest>();
        }

        public virtual int Id {
            get {
                return id;
            }
            set {
                this.id = value;
            }
        }
        public virtual int Rate {
            get {
                return rate;
            }
            set {
                this.rate = value;
            }
        }
        public virtual string Name {
            get {
                return name;
            }
            set {
                this.name = value;
            }
        }
        public virtual string Password {
            get {
                return password;
            }
            set {
                this.password = value;
            }
        }
        public virtual List<Contest> CurrentContests
        {
            get
            {
                return currentContests;
            }
            set
            {
                this.currentContests = value;
            }
        }

    }

}