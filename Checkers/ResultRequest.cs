﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace Checkers
{
    [JsonObject]
    public class Result {
        public bool State { set; get; }
        public string Message { set; get; }

    }
    [JsonObject]
    public class GameResult: Result {
        [JsonProperty("object")]
        public Game Game;

        public GameResult(bool state, string message, Game game)
        {
            this.State = state;
            this.Message = message;
            this.Game = game;
        }
    }

    [JsonObject]
    public class PlayerResult : Result {
        [JsonProperty("object")]
        public Player Player { set; get; }

        public PlayerResult(bool State, string Message, Player player) {
            this.State = State;
            this.Message = Message;
            this.Player = player;
        }
    }
    [JsonObject]
    public class MovesResult : Result {
        [JsonProperty("object")]
        public List<List<Action>> Paths { set; get; }

        public MovesResult(bool State , string Message , List<List<Action>> paths) {
            this.State = State;
            this.Message = Message;
            this.Paths = paths;

        }
    }
    public class ApplyResult : Result {
        [JsonProperty("object")]
        public List<Action> path;
        public ApplyResult(bool state, string message, List<Action> path) {
            this.State = state;
            this.Message = message;
            this.path = path;
        }
    }
    public class MessageResult : Result {
        [JsonProperty("object")]
        public string msg;
        public MessageResult(bool state, string message, string msg) {
            this.State = state;
            this.msg = msg;
            this.Message = message;
        }
    }
    [JsonObject]
    public class ContestResult : Result {
        [JsonProperty("object")]
        public List<Contest> Contests { set; get; }

        public ContestResult(bool State, string Message, List<Contest> Contests) {
            this.State = State;
            this.Message = Message;
            this.Contests = Contests;
        }
    }

    public class Message {
        public string msg { set; get; }
        public int Id { set; get; }
    }

    [JsonObject]
    public class GamesResult : Result {
        [JsonProperty("object")]
        public List<Game> Games { set; get; }

        public GamesResult(bool State, string Message, List<Game> Games) {
            this.State = State;
            this.Message = Message;
            this.Games = Games;
        }
    }
    [JsonObject]
    public class POVResult : Result {
        [JsonProperty("object")]
        public int POV;

        public POVResult(bool State, string Message, int POV) {
            this.State = State;
            this.Message = Message;
            this.POV = POV;
        }

    }

}
