﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers
{
    [JsonObject]
    public class InitRequest
    {
        public int Id { set; get; }
    }
}
