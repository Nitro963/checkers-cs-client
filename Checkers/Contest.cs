﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Checkers;
namespace Checkers {
    using Constraints;

    [JsonObject]
    public class Contest {
        private int id;
        private string name;
        private DateTime date;

        private Mode mode;

        public Contest(int id, string name, DateTime date, Mode mode) {
            this.id = id;
            this.name = name;
            this.date = date;
            this.mode = mode;
        }


        public virtual int Id {
            get {
                return id;
            }
            set {
                this.id = value;
            }
        }
        public virtual string Name {
            get {
                return name;
            }
            set {
                this.name = value;
            }
        }
        public virtual DateTime Date {
            get {
                return date;
            }
            set {
                this.date = value;
            }
        }
        public virtual Mode Mode {
            get {
                return mode;
            }
            set {
                this.mode = value;
            }
        }
    }
}