﻿using System;


namespace Checkers
{
    public class Form
    {
        public string name;
        public DateTime date;
        public Mode mode;
        public int maxRate;
        public int minRate;
        public int countOfPlayer;

        public Form(string name, DateTime date, Mode mode, int maxRate, int minRate, int countOfPlayer) {
            this.name = name;
            this.date = date;
            this.mode = mode;
            this.maxRate = maxRate;
            this.countOfPlayer = countOfPlayer;
            this.minRate = minRate;
        }

    }
}
