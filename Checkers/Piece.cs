﻿
using Newtonsoft.Json;

namespace Checkers {
	[JsonObject]
	public class Piece {
		private Cell cell;
		private Type type;
		private Color color;
		private int dead;

		public Piece(Cell cell, Type type, Color color) {
			this.cell = cell;
			this.type = type;
			this.color = color;
			dead = 0;
		}

		public virtual Cell Cell {
			get {
				return cell;
			}
			set {
				this.cell = value;
			}
		}
		public virtual Type Type {
			get {
				return type;
			}
			set {
				this.type = value;
			}
		}
		public virtual Color Color {
			get {
				return color;
			}
			set {
				this.color = value;
			}
		}
		public virtual int Dead {
			get {
				return dead;
			}
			set {
				this.dead = value;
			}
		}
	}

}