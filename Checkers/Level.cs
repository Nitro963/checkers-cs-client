﻿
namespace Checkers
{
    public enum Level
    {
        HUMAN,
        OFFLINE,
        DUMMY,
        ALPHA_BETA,
        MONTE_CARLO,
        ALPHA_ZERO
    }
}
