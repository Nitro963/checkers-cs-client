﻿using Newtonsoft.Json;

namespace Checkers {
    [JsonObject]
    public class Grid {
        private int n;
        private int m;
        private Cell[,] GridArr;

        public Grid(int n, int m) {
            this.n = n;
            this.m = m;

            GridArr = new Cell[n, m];

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    GridArr[i, j] = new Cell(i, j, null);
                }
            }
        }
        public virtual int N {
            set { n = value; }
            get { return n; }
        }
        public virtual int M {
            set { m = value; }
            get { return m; }
        }

        public Cell this[int i, int j] {
            set { GridArr[i, j] = value; }
            get { return GridArr[i, j]; }
        }

    }
}