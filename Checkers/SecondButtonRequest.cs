﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace Checkers
{
    [JsonObject]
    class SecondButtonRequest
    {
        public List<List<int>> Path { set; get; }
        public int Id { set; get; }

        public SecondButtonRequest(List<List<int>> path , int id) {
            this.Id = id;
            this.Path = path;
        }

    }
}
