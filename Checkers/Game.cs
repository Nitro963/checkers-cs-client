﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Checkers {

    [JsonObject]
    public class Game
    {
        public int Id { set; get; }
        public Mode Mode{set; get;}
        public Level Level { set; get; }
        public Grid Grid { set; get; }
        public Player Player1 { set; get; }
        public Player Player2 { set; get; }
        [JsonProperty("current_turn")]
        public int CurrentTurn { set; get; }
        [JsonProperty("no_progress_count")]
        public int NoProgressCount { set; get; }
        public DateTime Date { set; get; }
        [JsonProperty("white_pieces")]
        public List<Piece> WhitePieces { set; get; }
        [JsonProperty("black_pieces")]
        public List<Piece> BlackPieces { set; get; }

        [JsonConstructor]
        public Game(int Id, Grid Grid, Player Player1, Player Player2, int CurrentTurn, int NoProgressCount, DateTime Date, List<Piece> WhitePieces, List<Piece> BlackPieces, Mode Mode, Level Level)
        {
            this.WhitePieces = WhitePieces;
            this.BlackPieces = BlackPieces;
            this.Id = Id;
            this.Grid = Grid;
            this.Player1 = Player1;
            this.Player2 = Player2;
            this.CurrentTurn = CurrentTurn;
            this.NoProgressCount = NoProgressCount;
            this.Date = Date;
            this.Mode = Mode;
            this.Level = Level;
            foreach (Piece p in WhitePieces)
            {
                var r = p.Cell.R;
                var c = p.Cell.C;

                Grid[r, c].Piece = p;
                p.Cell = Grid[r, c];
            }
            foreach (Piece p in BlackPieces)
            {
                var r = p.Cell.R;
                var c = p.Cell.C;

                Grid[r, c].Piece = p;
                p.Cell = Grid[r, c];
            }
        }
    }
}

