﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace Checkers
{
    [JsonObject]
    class FirstButtonRequest
    {
        public int Id { set; get; }
        public int R { set; get; }
        public int C { set; get; }

        public FirstButtonRequest(int id , int r  , int c) {
            this.Id = id;
            this.C = c;
            this.R = r; 
        }
    }
}
