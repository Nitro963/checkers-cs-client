﻿namespace Checkers {
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [JsonConverter(typeof(StringEnumConverter))]
    public enum Mode {
        INTERNATIONAL, TURKISH
    }

}