﻿using Checkers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SocketManager : MonoBehaviour{
    public static JSONObject ToJson(object obj) {
        DefaultContractResolver contractResolver = new DefaultContractResolver {
            NamingStrategy = new SnakeCaseNamingStrategy()
        };
        string s = JsonConvert.SerializeObject(obj, new JsonSerializerSettings {
            ContractResolver = contractResolver,
            Formatting = Formatting.Indented
        });
        return JSONObject.Create(s);
    }

    public static SocketIO.SocketIOComponent socket;

    public void Awake() {
        if(socket != null) {
            DestroyImmediate(this);
            return;
        }
        socket = new SocketIO.SocketIOComponent();
        socket.Awake();
        DontDestroyOnLoad(this);
    }

    public void Start() {
        socket.Start();
        Init();
        SetupEvents();
        SceneManager.LoadScene("Login");
    }

    public void Update() {
        socket.Update();
    }

    public void OnDestroy() {
        socket.OnDestroy();
    }

    public void OnApplicationQuit() {
        socket.OnApplicationQuit();
    }

    public void SetupEvents() {
        socket.On("open", (E) => {
            Debug.Log("Connection made to the server");
        });
        socket.On("disconnect", (E) => {
            Debug.Log("Connection lost");
        });
        socket.On("message", (E) => {
            Debug.Log("the server says: " + E.data.ToString());
        });
    }

    public void Init() {

    }
}
