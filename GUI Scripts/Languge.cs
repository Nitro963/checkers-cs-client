﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Languge : MonoBehaviour
{
   
    // 0 for engilsh languge and 1 for french
    public static int Lang = 0;
    //Start New Game
    public static ArrayList OnePlayer = new ArrayList() { "One Player", "Un joueur" };
    public static ArrayList Easy = new ArrayList() { "Easy", "Facile" };
    public static ArrayList Medium = new ArrayList() { "Medium", "Moyen" };
    public static ArrayList Hard = new ArrayList() { "Hard", "Difficile" };
    public static ArrayList Back = new ArrayList() { "Back", "Retour" };
    public static ArrayList Online = new ArrayList() { "Online", "En ligne" };
    public static ArrayList TwoPlayer = new ArrayList() { "Two Player", "Deux joueurs" };
    public static ArrayList StartNewGame = new ArrayList() { "Start new game", "nouveau jeu" };
    //game Interface
    public static ArrayList Hint = new ArrayList() { "Hint", "Allusion" };
    public static ArrayList Save = new ArrayList() { "Save", "sauvegarder" };
    //option
    public static ArrayList Load = new ArrayList() { "Load", "Charge" };
    public static ArrayList Music = new ArrayList() { "Music", "musique" };
    public static ArrayList Exit = new ArrayList() { "Exit", "Sortie" };
    //Music
    public static ArrayList MusicName = new ArrayList() { "Music name", "Nom" };
    public static ArrayList MusicSound = new ArrayList() { "Music sound", "du son" };
    //GameType
    public static ArrayList StartOb = new ArrayList() { "Start", "Début" };
    public static ArrayList Player = new ArrayList() { "Player", "Joueuse" };
    public static ArrayList GameType = new ArrayList() { "Game Type", "Type de jeu" };
    public static ArrayList International = new ArrayList() { "International", "Internationale" };
    public static ArrayList Turkish = new ArrayList() { "Turkish", "Turque" };



    // Start is called before the first frame update
    void Start()
    {
       // print(OnePlayer[0]);
    }
}
