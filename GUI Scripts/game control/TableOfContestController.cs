﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class TableOfContestController : MonoBehaviour
{

    public ScrollRect scrollView;
    public GameObject scrollContent;
    public GameObject scrollItemPrefab;
    public GameObject detailsObj;

    public GameObject joinBtn;
    public static string nameOfContest;

    // Start is called before the first frame update
    void Start()
    {
        details();
        addSubject();
        for (int i = 1; i < 20; i++)
        {
            getrateItem(i);
        }
        scrollView.verticalNormalizedPosition = 1;

        

        //Username.SetText((string)userInformation.username);
    }

    public void details()
    {
        GameObject scrollItemObj = Instantiate(detailsObj);
        scrollItemObj.transform.SetParent(scrollContent.transform, false);
        scrollItemObj.transform.Find("sub").gameObject.GetComponent<Text>().text = "subj ";
        scrollItemObj.transform.Find("details").gameObject.GetComponent<Text>().text = "detailssss ";
    }

    public void addSubject()
    {
        GameObject scrollItemObj = Instantiate(scrollItemPrefab);
        scrollItemObj.transform.SetParent(scrollContent.transform, false);
    }
    public void getrateItem(int itemNumber)
    {
        GameObject scrollItemObj = Instantiate(scrollItemPrefab);
        scrollItemObj.transform.SetParent(scrollContent.transform, false);
        scrollItemObj.transform.Find("num").gameObject.GetComponent<Text>().text = itemNumber.ToString();
        scrollItemObj.transform.Find("Player1").gameObject.GetComponent<Text>().text = "player1 " + itemNumber.ToString();
        scrollItemObj.transform.Find("Player2").gameObject.GetComponent<Text>().text = "Player2 " + itemNumber.ToString();
        scrollItemObj.transform.Find("Winnar").gameObject.GetComponent<Text>().text = "Winar " + itemNumber.ToString();
        scrollItemObj.transform.Find("Date").gameObject.GetComponent<Text>().text = "Date  " + itemNumber.ToString();
       // scrollItemObj.GetComponent<Button>().onClick.AddListener(() => selectItemFromScrollBoard(scrollItemObj.GetComponent<Button>()));
    }

   /* public void selectItemFromScrollBoard(Button b)
    {
       // numberOfGame = b.transform.Find("num").gameObject.GetComponent<Text>().text;
       // SceneManager.LoadScene("SampleScene");
    }
    */
    public void goToProfileOnline()
    {
        SceneManager.LoadScene("ProfileOnline");
    }
    public void goToRuleOnline()
    {
        SceneManager.LoadScene("RuleOnline");
    }
    public void goToConfrontation()
    {
        SceneManager.LoadScene("Confrontation");
    }
    public void clickJoin() {
        //send to zaher the username and contest 
    }

}
