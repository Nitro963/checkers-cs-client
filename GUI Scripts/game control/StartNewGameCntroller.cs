﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using Checkers;
using Newtonsoft.Json;
public class StartNewGameCntroller : MonoBehaviour
{
    // languge
    public TextMeshProUGUI OnePlayer;
    public  TextMeshProUGUI Easy;
    public  TextMeshProUGUI Medium;
    public  TextMeshProUGUI Hard;
    public  TextMeshProUGUI Back;
    public  TextMeshProUGUI Online;
    public  TextMeshProUGUI TwoPlayer;
    public  TextMeshProUGUI StartNewGame;
    //end languge
    public GameInfo gameInfo;


    // Start is called before the first frame update
    void Start() {
        //languge
        OnePlayer.SetText((string)Languge.OnePlayer[Languge.Lang]);
        Easy.SetText((string)Languge.Easy[Languge.Lang]);
        Medium.SetText((string)Languge.Medium[Languge.Lang]);
        Hard.SetText((string)Languge.Hard[Languge.Lang]);
        Back.SetText((string)Languge.Back[Languge.Lang]);
        Online.SetText((string)Languge.Online[Languge.Lang]);
        TwoPlayer.SetText((string)Languge.TwoPlayer[Languge.Lang]);
        StartNewGame.SetText((string)Languge.StartNewGame[Languge.Lang]);
        //end langugu
        SocketManager.socket.On("create_new_game", LoadMainGame);
    }
    public void LoadMainGame(SocketIO.SocketIOEvent E) {
        GameResult rr = JsonConvert.DeserializeObject<GameResult>(E.data.ToString());
        if (rr.State) {
            BoardController.currentGame = rr.Game;
            SceneManager.LoadScene("MainGame");
        }
    }
    public void Awake() {
        DontDestroyOnLoad(this);
    }

    public void goToOption()
    {
        SceneManager.LoadScene("Option");
    }

    public void clickEasy()
    {
        gameInfo = new GameInfo() {
            mode = Mode.INTERNATIONAL,
            level = Level.MONTE_CARLO,
            init = false
        };
        Debug.Log("Emiting new game");
        SocketManager.socket.Emit("create_new_game", SocketManager.ToJson(gameInfo));
    }
    public void clickMedium()
    {
        gameInfo = new GameInfo() {
            mode = Mode.INTERNATIONAL,
            level = Level.ALPHA_BETA,
            init = false
        };
        Debug.Log("Emiting new game");
        SocketManager.socket.Emit("create_new_game", SocketManager.ToJson(gameInfo));
    }
    public void clickHard()
    {
        gameInfo = new GameInfo() {
            mode = Mode.INTERNATIONAL,
            level = Level.ALPHA_ZERO,
            init = false
        };
        Debug.Log("Emiting new game");
        SocketManager.socket.Emit("create_new_game", SocketManager.ToJson(gameInfo));
    }
    public void clickTwoPlayer()
    {
        gameInfo = new GameInfo() {
            mode = Mode.INTERNATIONAL,
            level = Level.OFFLINE,
            init = false
        };
        Debug.Log("Emiting new game");
        SocketManager.socket.Emit("create_new_game", SocketManager.ToJson(gameInfo));
    }
    public void clickOnline()
    {
        SceneManager.LoadScene("Confrontation");
    }


}
