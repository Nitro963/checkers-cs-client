﻿using UnityEngine;
using TMPro;
using Checkers;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
public class ProfileOnlineController : MonoBehaviour
{
    // public Text badad;

    public TextMeshProUGUI Rate;
    public TextMeshProUGUI Username;

    // Start is called before the first frame update
    void Start(){
        SocketManager.socket.On("show_account_result", showAccountRes);
        SocketManager.socket.On("remove_account_result", removeAccountRes);
        SocketManager.socket.Emit("show_account");
    }
    public void showAccountRes(SocketIO.SocketIOEvent response)
    {
        
        PlayerResult rr = JsonConvert.DeserializeObject<PlayerResult>(response.data.ToString());
        Debug.Log(rr.Message);
        if (rr.State)
        {
            Username.SetText((string)rr.Player.Name);
            Rate.SetText(rr.Player.Rate.ToString());
        }
    }


    void removeAccountRes(SocketIO.SocketIOEvent response)
    {
        PlayerResult rr = JsonConvert.DeserializeObject<PlayerResult>(response.data.ToString());

        Debug.Log(rr.Message);
        if (rr.State){
            SceneManager.LoadScene("Option");
        }
    }

    public void goToProfileOnline()
    {
        SceneManager.LoadScene("ProfileOnline");
    }
    public void goToRuleOnline()
    {
        SceneManager.LoadScene("RuleOnline");
    }
    public void goToConfrontation()
    {
        SceneManager.LoadScene("Confrontation");
    }

    public void goToContestNotCompleted()
    {
        ContestController.contestType = "notCompleted";
        SceneManager.LoadScene("ContestNotComplate");
    }
    public void goToMyContest()
    {
        ContestController.contestType = "myContest";
        SceneManager.LoadScene("Contest");
    }
    public void goToContestCompleted()
    {
        ContestController.contestType = "completed";
        SceneManager.LoadScene("FinishContest");
    }
    public void deleteAccount() {
        SocketManager.socket.Emit("remove_account");
    }
    public void goToCreateContest()
    {
        SceneManager.LoadScene("CreateContest");
    }
    public void goToSettings()
    {
        SceneManager.LoadScene("Settings");
    }

}
