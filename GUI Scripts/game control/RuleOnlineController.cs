﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RuleOnlineController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void goToProfileOnline()
    {
        SceneManager.LoadScene("ProfileOnline");
    }
    public void goToRuleOnline()
    {
        SceneManager.LoadScene("RuleOnline");
    }
    public void goToConfrontation()
    {
        SceneManager.LoadScene("Confrontation");
    }
    public void goToContestNotCompleted()
    {
        SceneManager.LoadScene("ContestNotComplate");
    }
    public void goToMyContest()
    {
        SceneManager.LoadScene("Contest");
    }
    public void goToContestCompleted()
    {
        SceneManager.LoadScene("Contest");
    }
}
