﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameTypeController : MonoBehaviour
{
    // languge
    public TextMeshProUGUI Back;
    public TextMeshProUGUI StartOb;
    public TextMeshProUGUI TurkishOb;
    public TextMeshProUGUI InternationalOb;
    public TextMeshProUGUI GameTypeOb;
    public TextMeshProUGUI Player1;
    public TextMeshProUGUI Player2;


    public Toggle internationa;
    public Toggle turkish;

    // Start is called before the first frame update
    void Start()
    {
        //languge
        Back.SetText((string)Languge.Back[Languge.Lang]);
        StartOb.SetText((string)Languge.StartOb[Languge.Lang]);
        TurkishOb.SetText((string)Languge.Turkish[Languge.Lang]);
        InternationalOb.SetText((string)Languge.International[Languge.Lang]);
        GameTypeOb.SetText((string)Languge.GameType[Languge.Lang]);
        Player1.SetText((string)Languge.Player[Languge.Lang] + " 1");
        Player2.SetText((string)Languge.Player[Languge.Lang]+ " 2");



        //Debug.Log(StartNewGameCntroller.playerType);
    }

    public void goToStartNewGame()
    {
        SceneManager.LoadScene("StartNewGame");
    }

    public void clickStart() {
        if (internationa.isOn)
            Gamee.gameType = "Internationa";
        else if (turkish.isOn)
            Gamee.gameType = "Turkish";
        SceneManager.LoadScene("SampleScene");
    }


   

}
