﻿    using UnityEngine;
using UnityEngine.UI;
using Checkers;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;

public class LoginController : MonoBehaviour{
    public InputField passObject;
    public InputField usernameObject;
    public GameObject wrongText;
    public Player player;

    void Start(){
        SocketManager.socket.On("login_result", LoginRes);
    }

    public void LoginRes(SocketIO.SocketIOEvent response){
        print("result");
        print(response.data.ToString());
        PlayerResult rr = JsonConvert.DeserializeObject<PlayerResult>(response.data.ToString());
        print("result");
        if (rr.State){
            SceneManager.LoadScene("Option");
        }
        else{
            Debug.Log(rr.Message);
            wrongText.SetActive(true);
        }
    }

    public void GoToSignup(){
        SceneManager.LoadScene("Signup");
    }
    
    public void ClickLogin() {
        
        player = new Player(usernameObject.text, passObject.text);
        
        SocketManager.socket.Emit("login", SocketManager.ToJson(player));
    }

    public void ClickBack(){
        SceneManager.LoadScene("StartNewGame");
    }
    
}
