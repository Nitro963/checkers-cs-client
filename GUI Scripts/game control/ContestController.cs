﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;


namespace Checkers
{
    public class ContestController : MonoBehaviour
    {
        public ScrollRect scrollView;
        public GameObject scrollContent;
        public GameObject scrollItemPrefab;

        public static string contestType;

        public ContestResult rr;
        public Contest contset;
        public bool temp = false;
        // Start is called before the first frame update
        void Start()
        {

            SocketManager.socket.On("show_my_current_contests_result", showMyCurrentContestsRes);
            SocketManager.socket.Emit("show_my_current_contests");

            //Username.SetText((string)userInformation.username);

        }

        void showMyCurrentContestsRes(SocketIO.SocketIOEvent response)
        {
            rr = JsonConvert.DeserializeObject<ContestResult>(response.data.ToString());
            Debug.Log(rr.Message);
            if (rr.State){
                foreach (Contest contest in rr.Contests)
                {
                    getrateItem(contest);
                }
            }
        }

        public void getrateItem(Contest contest)
        {
            GameObject scrollItemObj = Instantiate(scrollItemPrefab);
            scrollItemObj.transform.SetParent(scrollContent.transform, false);
            scrollItemObj.transform.Find("num").gameObject.GetComponent<Text>().text = contest.Id.ToString();
            scrollItemObj.transform.Find("GameType").gameObject.GetComponent<Text>().text = contest.Mode.ToString();
            scrollItemObj.transform.Find("Name").gameObject.GetComponent<Text>().text = contest.Name;
            scrollItemObj.transform.Find("Date").gameObject.GetComponent<Text>().text = contest.Date.ToUniversalTime().ToString();
            scrollItemObj.GetComponent<Button>().onClick.AddListener(() => selectItemFromScrollBoard(scrollItemObj.GetComponent<Button>()));
        }

        public void selectItemFromScrollBoard(Button b)
        {
            TableOfContestController.nameOfContest = b.transform.Find("Name").gameObject.GetComponent<Text>().text;
            SceneManager.LoadScene("TableOfContest");
        }

        public void goToProfileOnline()
        {
            SceneManager.LoadScene("ProfileOnline");
        }
        public void goToRuleOnline()
        {
            SceneManager.LoadScene("RuleOnline");
        }
        public void goToConfrontation()
        {
            SceneManager.LoadScene("Confrontation");
        }
        public void goToContestNotCompleted()
        {
            //contestType = "notCompleted";
            SceneManager.LoadScene("ContestNotComplate");
        }
        public void goToMyContest()
        {
            //contestType = "myContest";
            SceneManager.LoadScene("Contest");
        }
        public void goToContestCompleted()
        {
            //contestType = "completed";
            SceneManager.LoadScene("Contest");
        }


    }
}