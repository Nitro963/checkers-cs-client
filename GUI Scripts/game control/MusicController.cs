﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class MusicController : MonoBehaviour
{
    // languge
    public TextMeshProUGUI Music;
    public TextMeshProUGUI Back;
    public TextMeshProUGUI MusicSound;
    public TextMeshProUGUI MusicName;


    // Start is called before the first frame update
    void Start()
    {
        //langugu
        Music.SetText((string)Languge.Music[Languge.Lang]);
        Back.SetText((string)Languge.Back[Languge.Lang]);
        MusicSound.SetText((string)Languge.MusicSound[Languge.Lang]);
        MusicName.SetText((string)Languge.MusicName[Languge.Lang]);
    }

    public void goToOption()
    {
        SceneManager.LoadScene("Option");
    }
}
