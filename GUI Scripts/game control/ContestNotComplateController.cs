﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Checkers;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using System;


public class ContestNotComplateController : MonoBehaviour
{
    public ScrollRect scrollView;
    public GameObject scrollContent;
    public GameObject scrollItemPrefab;

    public static string contestType;

    public ContestResult rr;
    public Contest contset;
    public bool temp = false;
    public bool temp1 = false; //new
    ContestResult rr1; //new
    // Start is called before the first frame update
    void Start(){
        
        SocketManager.socket.On("show_all_contests_available_result", showMyContesshowAllContestsRes);
        SocketManager.socket.On("join_player_to_contest_result", joinPlayer2ContestRes); 

        SocketManager.socket.Emit("show_all_contests_available");

    }
    /*new*/
    void joinPlayer2ContestRes(SocketIO.SocketIOEvent response)
    {
        rr1 = JsonConvert.DeserializeObject<ContestResult>(response.data.ToString());
        if (rr1.State)
        {
            Debug.Log("joined");
            temp1 = true;
        }
    }
    /*End new*/

    void showMyContesshowAllContestsRes(SocketIO.SocketIOEvent response)
    {
        rr = JsonConvert.DeserializeObject<ContestResult>(response.data.ToString());
        Debug.Log(rr.Message);
        if (rr.State)
        {
            foreach (Contest contest in rr.Contests)
            {
                getrateItem(contest);
            }
        }
    }

    public void getrateItem(Contest contest)
    {
        GameObject scrollItemObj = Instantiate(scrollItemPrefab);
        scrollItemObj.transform.SetParent(scrollContent.transform, false);
        scrollItemObj.transform.Find("num").gameObject.GetComponent<Text>().text = contest.Id.ToString();
        scrollItemObj.transform.Find("GameType").gameObject.GetComponent<Text>().text = contest.Mode.ToString();
        scrollItemObj.transform.Find("Name").gameObject.GetComponent<Text>().text = contest.Name;
        scrollItemObj.transform.Find("Date").gameObject.GetComponent<Text>().text = contest.Date.ToUniversalTime().ToString();
        scrollItemObj.GetComponent<Button>().onClick.AddListener(() => selectItemFromScrollBoard(scrollItemObj.GetComponent<Button>()));
    }

    public void selectItemFromScrollBoard(Button b)
    {
        int id = Int32.Parse(b.transform.Find("num").gameObject.GetComponent<Text>().text); //new

        SocketManager.socket.Emit("join_player_to_contest", SocketManager.ToJson(new InitRequest() { Id = id }));
                   
        // TableOfContestController.nameOfContest = b.transform.Find("Name").gameObject.GetComponent<Text>().text;
        //SceneManager.LoadScene("TableOfContest");
    }

    public void goToProfileOnline()
    {
        SceneManager.LoadScene("ProfileOnline");
    }
    public void goToRuleOnline()
    {
        SceneManager.LoadScene("RuleOnline");
    }
    public void goToConfrontation()
    {
        SceneManager.LoadScene("Confrontation");
    }
    public void goToContestNotCompleted()
    {
        //contestType = "notCompleted";
        SceneManager.LoadScene("ContestNotComplate");
    }
    public void goToMyContest()
    {
        //contestType = "myContest";
        SceneManager.LoadScene("Contest");
    }
    public void goToContestCompleted()
    {
        //contestType = "completed";
        SceneManager.LoadScene("Contest");
    }


}
