﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using Checkers;

public class CreateConestController : MonoBehaviour
{
    public Toggle internationa;
    public Toggle turkish;

    public InputField contestName;
    public InputField playerCount;
    public InputField maxRateCondition;
    public InputField minRteCondition;
    public InputField day;
    public InputField month;
    public InputField year;
    public InputField hour;
    public InputField minute;
    public int gameType;
    public Form form;
    public bool temp = false;
    public GameObject wrongText;
    void Start()
    {
        SocketManager.socket.On("create_contest_result", createContestRes);
    }
    void createContestRes(SocketIO.SocketIOEvent response)
    {
        ContestResult rr = JsonConvert.DeserializeObject<ContestResult>(response.data.ToString());
        Debug.Log(rr.Message);
        if (rr.State)
        {
            SceneManager.LoadScene("ProfileOnline");
        }
        else
            wrongText.SetActive(true);
    }

    
    public void goToProfileOnline()
    {
        SceneManager.LoadScene("ProfileOnline");
    }

    public void clickCreate()
    {
        if (internationa.isOn)
            gameType = 1;
        else if (turkish.isOn)
            gameType = 2;

        DateTime date = new DateTime(int.Parse(year.text), int.Parse(month.text), int.Parse(day.text), int.Parse(hour.text), int.Parse(minute.text), 0);
        if (gameType == 1)
                form = new Form(contestName.text, date, Mode.INTERNATIONAL, int.Parse(maxRateCondition.text), int.Parse(minRteCondition.text) , int.Parse(playerCount.text));
        else
            form = new Form(contestName.text, date, Mode.TURKISH, int.Parse(maxRateCondition.text), int.Parse(minRteCondition.text), int.Parse(playerCount.text));

        SocketManager.socket.Emit("create_contest", SocketManager.ToJson(form));
    }

}
