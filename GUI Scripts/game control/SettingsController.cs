﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Checkers;
using SocketIOClient;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;

public class SettingsController : MonoBehaviour
{
    public GameObject wrongPassward;
    public InputField usernameObject;
    public InputField pass1Object;
    public InputField pass2Object;


    // Start is called before the first frame update
    void Start(){

        SocketManager.socket.On("update_account_result", updateAccountRes);

    }


    void updateAccountRes(SocketIO.SocketIOEvent response)
    {
        PlayerResult rr = JsonConvert.DeserializeObject<PlayerResult>(response.data.ToString());
        if (rr.State)
        {
            SceneManager.LoadScene("ProfileOnline");
        }
        else
        {
            wrongPassward.SetActive(true);
        }
    }
    public void goToProfile()
    {
        SceneManager.LoadScene("ProfileOnline");
    }
   /* public void clickUpdate() {
        SceneManager.LoadScene("ProfileOnline");
    }*/
    public void clickUpdate()
    {
        // send pass and username to zaher         
        if (!checKmatchPasswards(pass1Object.text, pass2Object.text) || pass1Object.text == "")
        {
            wrongPassward.SetActive(true);
        }

        // always will be false
        if (false && checKmatchPasswards(pass1Object.text, pass2Object.text) && !(pass1Object.text == ""))
        {
            wrongPassward.SetActive(true);
        }
        if (checKmatchPasswards(pass1Object.text, pass2Object.text) && !(pass1Object.text == ""))
        {
            SocketManager.socket.Emit("update_account", SocketManager.ToJson(new Player(usernameObject.text, pass1Object.text)));
        }
    }

    public bool checKmatchPasswards(string pass1, string pass2)
    {
        if (pass1 == pass2)
            return true;
        else
            return false;
    }



}
