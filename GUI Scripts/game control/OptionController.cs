﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class OptionController : MonoBehaviour
{
    // languge
    public TextMeshProUGUI Exit;
    public TextMeshProUGUI Load;
    public TextMeshProUGUI Music;
    public TextMeshProUGUI StartNewGAme;
    public GameObject sound;    


    // Start is called before the first frame update
    void Start()
    {
        //languge
        Exit.SetText((string)Languge.Exit[Languge.Lang]);
        Load.SetText((string)Languge.Load[Languge.Lang]);
        Music.SetText((string)Languge.Music[Languge.Lang]);
        StartNewGAme.SetText((string)Languge.StartNewGame[Languge.Lang]);
        DontDestroyOnLoad(sound);
    }

    public void goToStartNewGame()
    {
        SceneManager.LoadScene("StartNewGame");
    }

    public void goToMusic()
    {
        SceneManager.LoadScene("Music");
    }
    public void goToLoad()
    {
        SceneManager.LoadScene("Load");
    }



}

