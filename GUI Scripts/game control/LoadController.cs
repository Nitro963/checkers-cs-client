﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Checkers;
using SocketIOClient;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using System;

public class LoadController : MonoBehaviour
{

    public ScrollRect scrollView;
    public GameObject scrollContent;
    public GameObject scrollItemPrefab;

    // Start is called before the first frame update
    void Start()
    {
        SocketManager.socket.On("show_games_saved_result", showGameSavedRes);
        SocketManager.socket.On("load_result", load_next_scene);
        SocketManager.socket.Emit("show_games_saved");
    }
    void load_next_scene(SocketIO.SocketIOEvent response)
    {
        print(response.data.ToString());
        GameResult rr = JsonConvert.DeserializeObject<GameResult>(response.data.ToString());
        print(rr.Message);
        if (rr.State) {
            BoardController.currentGame = rr.Game;
            SceneManager.LoadScene("MainGame");
        }
    }

    void showGameSavedRes(SocketIO.SocketIOEvent response){
        GamesResult rr = JsonConvert.DeserializeObject<GamesResult>(response.data.ToString());
        if (rr.State){
            foreach (Game game in rr.Games)
            {
                getrateItem(game);
            }

        }
    }

    public void getrateItem(Game game)
    {
        GameObject scrollItemObj = Instantiate(scrollItemPrefab);
        scrollItemObj.transform.SetParent(scrollContent.transform, false);
        scrollItemObj.transform.Find("num").gameObject.GetComponent<Text>().text = game.Id.ToString();
        scrollItemObj.transform.Find("GameType").gameObject.GetComponent<Text>().text = game.Mode.ToString();
        scrollItemObj.transform.Find("Date").gameObject.GetComponent<Text>().text = game.Date.ToUniversalTime().ToString();
        scrollItemObj.GetComponent<Button>().onClick.AddListener(() => selectItemFromScrollBoard(scrollItemObj.GetComponent<Button>()));
    }

    public void selectItemFromScrollBoard(Button b)
    {
        int id = Int32.Parse(b.transform.Find("num").gameObject.GetComponent<Text>().text);
        SocketManager.socket.Emit("load", SocketManager.ToJson(new InitRequest() { Id = id }));
    }

    public void clickBack()
    {
        SceneManager.LoadScene("Option");
    }


}