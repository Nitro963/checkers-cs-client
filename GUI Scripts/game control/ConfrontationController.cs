﻿using UnityEngine;
using UnityEngine.UI;
using Checkers;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using System;

public class ConfrontationController : MonoBehaviour
{

    public ScrollRect scrollView;
    public GameObject scrollContent;
    public GameObject scrollItemPrefab;

    public Toggle internationa;
    public Toggle turkish;
    public static Mode gameType;
    public static string numberOfGame;


    //public TextMeshProUGUI Username;





    // Start is called before the first frame update
    void Start()
    {
        SocketManager.socket.On("all_waiting_games", allWaitingGames);
       SocketManager.socket.On("join", joinPlayer2Game);

        SocketManager.socket.Emit("all_waiting_games");
        //Username.SetText((string)userInformation.username);
    }
    void joinPlayer2Game(SocketIO.SocketIOEvent E) {
        GameResult rr = JsonConvert.DeserializeObject<GameResult>(E.data.ToString());
        Debug.Log(rr.Message);
        if (rr.State) {
            BoardController.currentGame = rr.Game;
            //StartNewGameCntroller.gameInfo = new GameInfo() { mode = rr.Game.Mode, init = false, level = Level.HUMAN };
            SceneManager.LoadScene("MainGame");
        }
    }
    void allWaitingGames(SocketIO.SocketIOEvent response)
    {
        Debug.Log(response.data.ToString());
        try {
            GamesResult rr = JsonConvert.DeserializeObject<GamesResult>(response.data.ToString());
            Debug.Log(rr.Message);
            if (rr.State) {
                foreach (Game game in rr.Games) {
                    getrateItem(game);
                }
            }
        }catch(Exception e) {
            Debug.Log(e.Message);
        }
    }

    public void getrateItem(Game game)
    {
        GameObject scrollItemObj = Instantiate(scrollItemPrefab);
        scrollItemObj.transform.SetParent(scrollContent.transform, false);
        scrollItemObj.transform.Find("num").gameObject.GetComponent<Text>().text = game.Id.ToString();
        scrollItemObj.transform.Find("playerName").gameObject.GetComponent<Text>().text = game.Player1.Name.ToString();
        scrollItemObj.transform.Find("GameType").gameObject.GetComponent<Text>().text = game.Mode.ToString();
        scrollItemObj.GetComponent<Button>().onClick.AddListener(() => selectItemFromScrollBoard(scrollItemObj.GetComponent<Button>()));
    }

    public void selectItemFromScrollBoard(Button b){   
        numberOfGame = b.transform.Find("num").gameObject.GetComponent<Text>().text;

        SocketManager.socket.Emit("join", SocketManager.ToJson(new InitRequest() { Id = int.Parse(numberOfGame) }));
    }


    public void goToProfileOnline()
    {
        SceneManager.LoadScene("ProfileOnline");
    }
    public void goToRuleOnline()
    {
        SceneManager.LoadScene("RuleOnline");
    }
    public void goToConfrontation()
    {
        SceneManager.LoadScene("Confrontation");
    }
    public void clickNewGame()
    {

        if (internationa.isOn)
            gameType = Mode.INTERNATIONAL;
        else if (turkish.isOn)
            gameType = Mode.TURKISH;

        SocketManager.socket.Emit("create_new_game", 
            SocketManager.ToJson(new GameInfo() { init = false, level = Level.HUMAN, mode = gameType}));

    }
    public void goToContestNotCompleted(){
        SceneManager.LoadScene("Contest");
    }
    public void goToMyContest(){
        SceneManager.LoadScene("Contest");
    }
    public void goToContestCompleted(){
            SceneManager.LoadScene("Contest");
    }

}
