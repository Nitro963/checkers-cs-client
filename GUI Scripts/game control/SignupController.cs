﻿using UnityEngine;
using UnityEngine.UI;
using Checkers;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;

public class SignupController : MonoBehaviour
{

    public GameObject wrongUsername;
    public GameObject wrongPassward;
    public InputField usernameObject;
    public InputField pass1Object;
    public InputField pass2Object;

    // Start is called before the first frame update
    void Start()
    {
        SocketManager.socket.On("signup_result", signUpRes);
    }
    public void signUpRes(SocketIO.SocketIOEvent response)
    {
        PlayerResult rr = JsonConvert.DeserializeObject<PlayerResult>(response.data.ToString());
        Debug.Log(rr.Message);
        if (rr.State)
            SceneManager.LoadScene("Option");
        else
            wrongPassward.SetActive(true);
    }
    public void goToLogin()
    {
        SceneManager.LoadScene("Login");
    }

    public void clickCreate()
    {
        // send pass and username to zaher         
        if (!checKmatchPasswards(pass1Object.text, pass2Object.text) || pass1Object.text == "")
        {
            wrongPassward.SetActive(true);
        }

        // always will be false
        if (false && checKmatchPasswards(pass1Object.text, pass2Object.text) && !(pass1Object.text == ""))
        {
            wrongPassward.SetActive(true);
        }
        if (checKmatchPasswards(pass1Object.text, pass2Object.text) && !(pass1Object.text == ""))
        {
            SocketManager.socket.Emit("signup", SocketManager.ToJson(new Player(usernameObject.text, pass1Object.text)));
        }
    }

    public bool checKmatchPasswards(string pass1, string pass2)
    {
        if (pass1 == pass2)
            return true;
        else
            return false;
    }
}
